%% Préparation des données
data=readtable('BM2017.csv');
CO2=table2array(data(:,3)); %CO2 salon
bruit=table2array(data(:,4));
time=table2array(data(:,2));
temp=table2array(data(:,10));

t=time-time(1);
t=t./60; %Vecteur temps
dCO2=diff(CO2)./diff(t);

day=144; week=1008; month=4320; %Nombre d'éléments par période

k_comp = 1; %Coefficient de sous-échantillonage

day_comp = 144/k_comp; week_comp = day_comp*7; month_comp = week_comp*4; h12_comp = day_comp/2;

CO2_comp = CO2(1:k_comp:end);
bruit_comp = bruit(1:k_comp:end);
temp_comp = temp(1:k_comp:end);
pression_comp = pression(1:k_comp:end);
humidite_comp = humidite(1:k_comp:end);

%% Apprentissage
response_size=day_comp;
predictor_size=week_comp;
data_size=predictor_size+response_size;

%Vecteurs d'entrainement synchronisés à la même date (1er Avril)
data_test=[CO2_comp(3*month_comp-predictor_size+1:3*month_comp+response_size),...
    bruit_comp(3*month_comp-predictor_size+1:3*month_comp+response_size),...
    pression_comp(3*month_comp-predictor_size+1:3*month_comp+response_size),...
    humidite_comp(3*month_comp-predictor_size+1:3*month_comp+response_size)];

numTimeStepsTrain = length(data_test)-response_size;

dataTrain = data_test(1:numTimeStepsTrain,:);
responseTrain = data_test(numTimeStepsTrain+1:end,:);

%Normalisation
means = mean(dataTrain,1);
sigs = std(dataTrain,0,1);
dataTrainStandardized = (dataTrain - means) ./ sigs;
responseTrainStandardized = (responseTrain - means) ./ sigs;

%Paramètres de l'entrainement
XTrain = dataTrainStandardized; XTrain=XTrain(:);
YTrain = responseTrainStandardized; YTrain=YTrain(:);
numFeatures = numel(XTrain);
numResponses = numel(YTrain);
numHiddenUnits = 2*numel(YTrain);

miniBatchSize = 1;

layers = [ ...
    sequenceInputLayer(numFeatures)
    bilstmLayer(numHiddenUnits,'OutputMode','sequence')
    fullyConnectedLayer(numResponses)
    regressionLayer];

options = trainingOptions('adam', ...
    'MaxEpochs',250, ...
    'MiniBatchSize',miniBatchSize, ...
    'GradientThreshold',1, ...
    'InitialLearnRate',0.005, ...
    'LearnRateSchedule','piecewise', ...
    'Verbose',0, ...
    'Plots','training-progress');
net = trainNetwork(XTrain,YTrain,layers,options);

%% Graphe Apprentissage
[net,Yapp] = predictAndUpdateState(net,XTrain);

CO2_app=sigs(1)*Yapp(1:response_size,:)+means(1); CO2_app=CO2_app(:);
bruit_app=sigs(2)*Yapp(response_size+1:2*response_size,:)+means(2); bruit_app=bruit_app(:);
pression_app=sigs(3)*Yapp(2*response_size+1:3*response_size,:)+means(3); pression_app=pression_app(:);
humidite_app=sigs(4)*Yapp(3*response_size+1:4*response_size,:)+means(4); humidite_app=humidite_app(:);

data_proto=[CO2_app,bruit_app,pression_app,humidite_app];

YTest=responseTrain(:,3); %A modifier selon la variable voulue
YPred=pression_app;
rmse=sqrt(nanmean((YPred-YTest).^2));

figure
subplot(2,1,1)
plot(YTest)
hold on
plot(YPred,'.-')
hold off
legend(["Observations" "Prédiction"])
title("Comparaison entre les valeurs prédites et les données réelles")

subplot(2,1,2)
stem(YPred - YTest)
xlabel("Observations")
ylabel("Erreur")
title("RMSE = " + rmse)


%% Test
[net,Ypred] = predictAndUpdateState(net,XTrain);

%Préparation de la fenêtre glissante
new_train=[[XTrain((response_size+1):(predictor_size)); 
    XTrain(((predictor_size)+response_size+1):(predictor_size)*2); 
    XTrain(((predictor_size)*2+response_size+1):(predictor_size)*3); 
    XTrain(((predictor_size)*3+response_size+1):(predictor_size)*4)]; 
    YTrain];

YPred = [];
[net,YPred] = predictAndUpdateState(net,new_train);

limit=30; %Nombre de prédictions à faire
for i = 2:limit
    if mod(i,3)==0
        new_train = [(CO2_comp(3*month_comp+response_size*i+1:(3*month_comp+numTimeStepsTrain+response_size*i))-means(1))/sigs(1); 
            (bruit_comp(3*month_comp+response_size*i+1:(3*month_comp+numTimeStepsTrain+response_size*i))-means(2))/sigs(2); 
            (pression_comp(3*month_comp+response_size*i+1:(3*month_comp+numTimeStepsTrain+response_size*i))-means(3))/sigs(3);
            (humidite_comp(3*month_comp+response_size*i+1:(3*month_comp+numTimeStepsTrain+response_size*i))-means(4))/sigs(4)];

    else
        new_train=[[new_train((response_size+1):(predictor_size)); 
            new_train(((predictor_size)+response_size+1):(predictor_size)*2); 
            new_train(((predictor_size)*2+response_size+1):(predictor_size)*3);
            new_train(((predictor_size)*3+response_size+1):(predictor_size)*4)];
            YPred(:,i-1)];
    end
    
   [net,YPred(:,i)] = predictAndUpdateState(net,new_train);

end

output=0; %CO2=0 / bruit=1 / pression=2 / humidite=3

CO2_pred=sigs(1)*YPred(1:response_size,:)+means(1); CO2_pred=CO2_pred(:);
bruit_pred=sigs(2)*YPred(response_size+1:2*response_size,:)+means(2); bruit_pred=bruit_pred(:);
pression_pred=sigs(3)*YPred(2*response_size+1:3*response_size,:)+means(3); pression_pred=pression_pred(:);
humidite_pred=sigs(4)*YPred(3*response_size+1:4*response_size,:)+means(4); humidite_pred=humidite_pred(:);

data_pred=[CO2_pred,bruit_pred,pression_pred,humidite_pred];


CO2_test = CO2_comp((3*month_comp+1):((3*month_comp+response_size*i)));
bruit_test = bruit_comp((3*month_comp+1):((3*month_comp+response_size*i)));
pression_test = pression_comp((3*month_comp+1):((3*month_comp+response_size*i)));
humidite_test = humidite_comp((3*month_comp+1):((3*month_comp+response_size*i)));

rmse_CO2 = sqrt(nanmean((CO2_pred-CO2_test).^2));
rmse_bruit = sqrt(nanmean((bruit_pred-bruit_test).^2));
rmse_pression = sqrt(nanmean((pression_pred-pression_test).^2));
rmse_humidite = sqrt(nanmean((humidite_pred-humidite_test).^2));


%% Graphes

%Comparaison entre prédictions et valeurs réelles
YTest=CO2_test;
YPred=CO2_pred;
rmse=sqrt(nanmean((YPred-YTest).^2));

grandeur="CO2";

figure
subplot(2,1,1)
plot(YTest)
hold on
plot(YPred,'.-')
hold off
set(gca,'XTick',0:24:numel(YPred))
set(gca,'XTickLabel',0:1:limit)
xlabel("Jours")
ylabel(grandeur)
legend(["Observations" "Prédiction"])
title("Comparaison entre les valeurs prédites et les données de test")

subplot(2,1,2)
stem(YPred - YTest)
xlabel("Observations")
ylabel("Erreur")
title("RMSE = " + rmse)

%Classification présence/absence binaire
pres=  fonction_matrices_solution(data_pred);
idx_pres=find(round(pres)==1);
idx_abs=find(round(pres)==0);
presence=bruit_pred;
presence(idx_abs)=NaN;
absence=bruit_pred;
absence(idx_pres)=NaN;

figure
plot(t,bruit_pred(1:day_comp*15),'b'),hold on
plot(t,presence(1:day_comp*15),'r'),hold off
set(gca,'XTick',0:day_comp:day_comp*15)
set(gca,'XTickLabel',0:1:15)
xlabel("Jours")
ylabel("bruit")
title("Prédiction du bruit pour 2 semaines")
legend(["Absence" "Présence"])

%Classification présence/absence avec incertitude
pres=  fonction_matrices_solution(data_pred);
t=1:numel(bruit_pred(1:day_comp*15));
y=bruit_pred(1:day_comp*15).';
z=zeros(size(t));
col=pres(1:day_comp*15).';

surface([t;t],[y;y],[z;z],[col;col],...
        'facecol','no',...
        'edgecol','interp',...
        'linew',2);
set(gca,'XTick',0:day_comp:day_comp*15)
set(gca,'XTickLabel',0:1:15)
xlabel("Jours")
ylabel("bruit")
title("Prédiction du bruit pour 2 semaines")








