%% Préparation des données
data=readtable('BM2017.csv');
CO2=table2array(data(:,3)); %CO2 salon
bruit=table2array(data(:,4));
time=table2array(data(:,2));
temp=table2array(data(:,10));

t=time-time(1);
t=t./60; %Vecteur temps
dCO2=diff(CO2)./diff(t);

day=144; week=1008; month=4320; %Nombre d'éléments par période

k_comp = 1; %Coefficient de sous-échantillonage

%Sous-échantillonage
CO2_comp = CO2(1:k_comp:end);
day_comp = 144/k_comp; week_comp = day_comp*7; month_comp = week_comp*4; 

%% Apprentissage
predictor_size=3*week_comp; %Nombre de données en entrée
response_size=7*day_comp; %Nombre de données à prédire
test_column=CO2_comp(1:predictor_size); %Vecteur d'entrainement

numTimeStepsTrain = numel(test_column(1:end-response_size)); %Nombre d'éléments à utiliser pour l'entrainement

dataTrain = test_column(1:numTimeStepsTrain); %Entrée
responseTrain = test_column(numTimeStepsTrain+1:end); %Sortie

%Normalisation
mu = mean(test_column);
sig = std(test_column);
dataTrainStandardized = (dataTrain - mu) / sig;
responseTrainStandardized = (responseTrain - mu) / sig;

%Paramètres de l'entrainement
XTrain = dataTrainStandardized;
YTrain = responseTrainStandardized;
numFeatures = numel(XTrain); %Nombre de caractéristiques = Nombre d'éléments dans le vecteur entrée
numResponses = numel(YTrain);
numHiddenUnits = 2*numel(YTrain); %Nombre d'états cachés = multiple du nombre de données à prédire (en général)
miniBatchSize = 1;

layers = [ ...
    sequenceInputLayer(numFeatures)
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    fullyConnectedLayer(numResponses)
    regressionLayer];
%Couches du réseau, modifier lstmLayer par bilstmLayer si besoin

options = trainingOptions('adam', ...
    'MaxEpochs',250, ...
    'MiniBatchSize',miniBatchSize, ...
    'GradientThreshold',1, ...
    'InitialLearnRate',0.005, ...
    'LearnRateSchedule','piecewise', ...
    'LearnRateDropPeriod',1750, ...
    'LearnRateDropFactor',0.2, ...
    'Verbose',0, ...
    'Plots','training-progress');
[net,info] = trainNetwork(XTrain,YTrain,layers,options);

%% Test
net = predictAndUpdateState(net,XTrain); %Première prédiction sur le vecteur d'entrainement

%Préparation de la fenêtre glissante
new_train=[XTrain(response_size+1:end); YTrain];
YPred = [];
[net,YPred(:,1)] = predictAndUpdateState(net,new_train);

limit=30; %Nombre de prédictions à faire
for i = 2:limit
    if mod(i,3)==0 %Tous les 3 jours = Mise à jour
        net = resetState(net);
        net = predictAndUpdateState(net,XTrain);
        new_train = (CO2_comp(response_size*i+1:(numTimeStepsTrain+response_size*i))-mu)/sig;
    else
        new_train=[new_train(response_size+1:end);YPred(:,i-1)];
    end
   [net,YPred(:,i)] = predictAndUpdateState(net,new_train);

end

YPred = YPred(:);
YPred = sig*YPred + mu;


YTest = CO2_comp((numTimeStepsTrain+1):((numTimeStepsTrain+response_size*i))); %Vrais valeurs synchronisées aux résultats de la prédiction
rmse = sqrt(nanmean((YPred-YTest).^2)) %nanmean car il y a parfois des trous de données

%% Graphes

figure
plot(CO2(1:week))
hold on
idx = (week+1):(week+day*1);
plot(idx,CO2(idx),'.-')
hold off
title("Prédiction")
legend(["Observé" "Prédiction"])
set(gca,'XTick',0:144:numel(CO2(1:week+day)))
set(gca,'XTickLabel',0:1:8)
xlabel("Jours")
ylabel("CO2")

figure
%Comparaison prédiction et valeurs réelles
subplot(2,1,1)
plot(YTest)
hold on
plot(YPred,'.-')
hold off
set(gca,'XTick',0:144:numel(YPred))
set(gca,'XTickLabel',0:1:limit)
xlabel("Jours")
ylabel("CO2")

%RMSE
subplot(2,1,2)
stem(YPred - YTest)
xlabel("Observations")
ylabel("Erreur")
title("RMSE = " + rmse1)

%Droites pour noter les jours de mise à jour de la fenêtre glissante
for i=1:limit
    if mod(i,3)==0
        xline(i*day)
    end
end
       
legend(["Observations" "Prédiction" "Mise à jour"])
title("Comparaison entre les valeurs prédites et les données de test")


