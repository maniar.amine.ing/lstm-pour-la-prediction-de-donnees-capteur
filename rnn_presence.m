%% Préparation des données
data=readtable('BM2017.csv');
CO2=table2array(data(:,3)); %CO2 salon
bruit=table2array(data(:,4));
time=table2array(data(:,2));
temp=table2array(data(:,10));

t=time-time(1);
t=t./60; %Vecteur temps
dCO2=diff(CO2)./diff(t);

day=144; week=1008; month=4320; %Nombre d'éléments par période

k_comp = 1; %Coefficient de sous-échantillonage

day_comp = 144/k_comp; week_comp = day_comp*7; month_comp = week_comp*4; h12_comp = day_comp/2;

CO2_comp = CO2(1:k_comp:end);
bruit_comp = bruit(1:k_comp:end);
temp_comp = temp(1:k_comp:end);
pression_comp = pression(1:k_comp:end);
humidite_comp = humidite(1:k_comp:end);

%% Etiquittage des données
response_size=day_comp;
predictor_size=week_comp;
data_size=predictor_size+response_size;

data_test=[CO2_comp(1:data_size),bruit_comp(1:data_size),pression_comp(1:data_size),humidite_comp(1:data_size)];
presence=data_test(:,1)>500 & data_test(:,2)>36;
data_test=cat(2,data_test,presence);

dataTrain = data_test(:,1:4); %Vecteur d'apprentissage
responseTrain = data_test(:,5); %Sortie

%Entrainement et modèle générés par Matlab. Appeler le modèle obtenu avec
%la fonction suivante

pres=  fonction_matrices_solution(data_Train);

